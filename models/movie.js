const Joi = require("joi");

/**
 * Validation for Customer
 * @param movie
 * @returns {Joi.ValidationResult<any>}
 */
function validateMovie(movie) {
    const schema = Joi.object({
        title: Joi.string().min(3).required(),
        genre: Joi.string().min(3).required()
    })
    return schema.validate(movie);
}

module.exports = validateMovie;