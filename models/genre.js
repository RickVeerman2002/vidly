const Joi = require("joi");

/**
 * Validation for Genre
 * @param genre
 * @returns {Joi.ValidationResult<any>}
 */
function validateGenre(genre) {
    const schema = Joi.object({
        name: Joi.string().min(3).required()
    })
    return schema.validate(genre);
}

module.exports = validateGenre;