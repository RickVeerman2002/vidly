const Joi = require("joi");

/**
 * Validation for Customer
 * @param rental
 * @returns {Joi.ValidationResult<any>}
 */
function validateRental(rental) {
    const schema = Joi.object({
        movie: Joi.string().min(3).required(),
        customerId: Joi.required(),
        dateReturned : Joi.date(),
        rentalFee : Joi.required()
    })
    return schema.validate(rental);
}

module.exports = validateRental;