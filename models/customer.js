const Joi = require("joi");

/**
 * Validation for Customer
 * @param customer
 * @returns {Joi.ValidationResult<any>}
 */
function validateCustomer(customer) {
    const schema = Joi.object({
        name: Joi.string().min(3).required()
    })
    return schema.validate(customer);
}

module.exports = validateCustomer;