const validate = require('../models/genre')
const express = require('express');
const router = express.Router();
const db = require('../database');


// Get all genres
router.get('/', async (req, res) => {
    try {
        // Query | All Genres
        const selectGenres = await db.promise().query("SELECT * FROM `genre`");

        const genres = selectGenres[0];
        //  Error Message | There are no genres
        if (genres.length < 1) return res.status(404).send('There were no genres found...');

        // Ok Message | All Genres
        res.status(200).send(genres);
    } catch (err) {
        res.send(new Error("Error: " + err.message));
    }
});

// Get a Genre
router.get('/:id', async (req, res) => {
    try {
        const genreId = req.params.id;
        // Query | Get one Genre with the genreId
        const selectGenre = await db.promise().query("SELECT * FROM `genre` WHERE id =?", [genreId]);

        const genre = selectGenre[0];

        //  Error Message | There is no genre
        if (genre.length < 1) return res.status(404).send('The genre with that given ID was not found...');

        // Ok Message | One Genre
        res.status(200).send(genre[0])
    } catch (err) {
        res.send(new Error("Error: " + err.message))
    }
});

// Create a Genre
router.post('/', async (req, res) => {
    try {
        const genreInput = req.body;
        // Validate Error
        const {error} = validate(genreInput)
        // Error Message | If validateGenre has a error
        if (error) return res.status(400).send(error.details[0].message);

        // Query | Insert a Genre
        const insertGenre = await db.promise().query("INSERT INTO `genre` (`name`) VALUES (?)", [genreInput["name"]]);

        const insertId = insertGenre[0].insertId;
        // Query | Select a Genre with the insertId
        const selectGenre = await db.promise().query("SELECT * FROM `genre` WHERE id = ?", [insertId]);

        const genre = selectGenre[0];

        //  Error Message | There is no Genre
        if (genre.length < 1) return res.status(404).send('The genre with that given ID was not found...');

        // Ok Response | New Genre
        res.status(200).send(genre[0])

    } catch (err) {
        res.send(new Error("Error: " + err.message));
    }
});

// Update a genre
router.put('/:id', async (req, res) => {
    try {
        const genreInput = req.body;
        const genreId = req.params.id;

        // Validate Error
        const {error} = validate(genreInput)
        // Error Message | If validateGenre has a error
        if (error) return res.status(400).send(error.details[0].message);

        // Query | Select a genre with the genreId
        const selectGenre = await db.promise().query("SELECT * FROM `genre` WHERE id = ?", [genreId]);
        const genre = selectGenre[0];
        //  Error Message | There is no genre
        if (genre.length < 1) return res.status(404).send('The genre with that given ID was not found...');

        // Query | Update a genre
        await db.promise().query("UPDATE `genre` SET `name` = ? WHERE id = ?", [genreInput["name"],  genreId]);

        // Query | Get one genre with the genreId
        const selectGenre_ = await db.promise().query("SELECT * FROM `genre` WHERE id = ?", [genreId]);
        const genre_ = selectGenre_[0];

        //  Error Message | There is no genre
        if (genre_.length < 1) return res.status(404).send('The genre with that given ID was not found...');

        // Ok Response | Updated Course
        res.status(200).send(genre_[0])
    } catch (err) {
        res.send(new Error("Error: " + err.message));
    }
});

// Delete a genre
router.delete('/:id', async (req, res) => {
    try {
        const genreId = req.params.id;

        // Query | Select a genre with the genreId
        const selectGenre = await db.promise().query("SELECT * FROM `genre` WHERE id = ?", [genreId]);
        const genre = selectGenre[0];
        //  Error Message | There is no genre
        if (genre.length < 1) return res.status(404).send('The genre with that given ID was not found...');

        // Query | Delete a genre with the genreId
        await db.promise().query("DELETE FROM `genre`  WHERE id = ?", [genreId]);

        // Ok Response | Deleted Genre
        res.status(200).send("Genre Deleted")
    } catch (err) {
        res.send(new Error("Error: " + err.message));
    }
})

module.exports = router;