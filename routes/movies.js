const validate = require('../models/movie');
const express = require('express');
const router = express.Router();
const db = require('../database');


// Get all Movies
router.get('/', async (req, res) => {
    try {
        // Query | All Movies
        const selectMovies = await db.promise().query("SELECT * FROM `movie`");

        const movies = selectMovies[0];
        //  Error Message | There are no Movies
        if (movies.length < 1) return res.status(404).send('There were no movies found...');

        // Ok Message | All Movies
        res.status(200).send(movies);
    } catch (err) {
        res.send(new Error("Error: " + err.message));
    }
});

// Get a Movie
router.get('/:id', async (req, res) => {
    try {
        const movieId = req.params.id;
        // Query | Get one Movie with the movieId
        const selectMovie = await db.promise().query("SELECT * FROM `movie` WHERE id = ?", [movieId]);

        const movie = selectMovie[0];

        //  Error Message | There is no Movie
        if (movie.length < 1) return res.status(404).send('The movie with that given ID was not found...');

        // Ok Message | One Movie
        res.status(200).send(movie[0])
    } catch (err) {
        res.send(new Error("Error: " + err.message))
    }
});

// Create a Movie
router.post('/', async (req, res) => {
    try {
        const movieInput = req.body;
        // Validate Error
        const {error} = validate(movieInput)
        // Error Message | If validateMovie has a error
        if (error) return res.status(400).send(error.details[0].message);

        await genreExists(movieInput, res);

        // Query | Insert a Movie
        const insertMovie = await db.promise().query("INSERT INTO `movie` (`title`, `genre`) VALUES (?, ?)", [movieInput["title"], movieInput["genre"]]);

        const insertId = insertMovie[0].insertId;
        // Query | Select a Movie with the insertId
        const selectMovie = await db.promise().query("SELECT * FROM `movie` WHERE id = ?", [insertId]);

        const movie = selectMovie[0];

        //  Error Message | There is no Movie
        if (movie.length < 1) return res.status(404).send('The movie with that given ID was not found...');

        // Ok Response | New Movie
        res.status(200).send(movie[0])

    } catch (err) {
        res.send(new Error("Error: " + err.message));
    }
});

// Update a Movie
router.put('/:id', async (req, res) => {
    try {
        const movieInput = req.body;
        const movieId = req.params.id;

        // Validate Error
        const {error} = validate(movieInput)
        // Error Message | If validateCustomer has a error
        if (error) return res.status(400).send(error.details[0].message);

        // Query | Select a Movie with the customerId
        const selectMovie = await db.promise().query("SELECT * FROM `movie` WHERE id = ?", [movieId]);
        const movie = selectMovie[0];
        //  Error Message | There is no customer
        if (movie.length < 1) return res.status(404).send('The movie with that given ID was not found...');

        await genreExists(movieInput, res);

        // Query | Update a Movie
        await db.promise().query("UPDATE `movie` SET `title` = ?, genre = ? WHERE id = ?", [movieInput["title"], movieInput["genre"],  movieId]);

        // Query | Get one Movie with the customerId
        const selectMovie_ = await db.promise().query("SELECT * FROM `movie` WHERE id = ?", [movieId]);
        const movie_ = selectMovie_[0];

        //  Error Message | There is no Movie
        if (movie_.length < 1) return res.status(404).send('The movie with that given ID was not found...');

        // Ok Response | Updated Movie
        res.status(200).send(movie_[0])
    } catch (err) {
        res.send(new Error("Error: " + err.message));
    }
});

// Delete a Movie
router.delete('/:id', async (req, res) => {
    try {
        const movieId = req.params.id;

        // Query | Select a Movie with the movieId
        const selectMovie = await db.promise().query("SELECT * FROM `genre` WHERE id = ?", [movieId]);
        const movie = selectMovie[0];
        //  Error Message | There is no Movie
        if (movie.length < 1) return res.status(404).send('The movie with that given ID was not found...');

        // Query | Delete a Movie with the movieId
        await db.promise().query("DELETE FROM `movie`  WHERE id = ?", [movieId]);

        // Ok Response | Deleted Movie
        res.status(200).send("Movie Deleted")
    } catch (err) {
        res.send(new Error("Error: " + err.message));
    }
})



async function genreExists(movieInput, res) {
    // Query | Select a Genre with the genreId
    const selectGenre = await db.promise().query("SELECT * FROM `genre` WHERE `name` = ?", [movieInput["genre"]]);
    const genre = selectGenre[0];
    //  Error Message | There is no Movie
    if (genre.length < 1) return res.status(404).send('The genre with the name: ' + movieInput["genre"] + "doesn't exist...");
}



module.exports = router;