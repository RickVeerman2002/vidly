const validate = require('../models/rental');
const express = require('express');
const router = express.Router();
const db = require('../database');


// Get all Rentals
router.get('/', async (req, res) => {
    try {
        // Query | All Rentals
        const selectRentals = await db.promise().query("SELECT * FROM `rental`");

        const rentals = selectRentals[0];
        //  Error Message | There are no Rentals
        if (rentals.length < 1) return res.status(400).send('There were no rentals found...');

        // Ok Message | All Rentals
        res.status(200).send(rentals);
    } catch (err) {
        res.send(new Error("Error: " + err.message));
    }
});

// Get a Rental
router.get('/:movie/:customerId', async (req, res) => {
    try {
        const movie = req.params.movie;
        const customerId = req.params.customerId;
        // Query | Get one Rental with the rentalId
        const selectRental = await db.promise().query("SELECT * FROM `rental` WHERE movie = ? AND customerId = ?", [movie, customerId]);

        const rental = selectRental[0];

        //  Error Message | There is no Rental
        if (rental.length < 1) return res.status(400).send('The rental with that given ID was not found...');

        // Ok Message | One Rental
        res.status(200).send(rental[0])
    } catch (err) {
        res.send(new Error("Error: " + err.message))
    }
});

// Create a Rental
router.post('/', async (req, res) => {
    try {
        const rentalInput = req.body;

        // Validate Error
        const {error} = validate(rentalInput)
        // Error Message | If validateRental has a error
        if (error) return res.status(400).send(error.details[0].message);

        await movieExists(rentalInput, res);
        await customerExists(rentalInput, res);

        const selectRentalCheck = await db.promise().query("SELECT * FROM `rental` WHERE movie = ? AND customerId = ?", [rentalInput["movie"], rentalInput["customerId"]]);
        const rentalCheck = selectRentalCheck[0];
        // If the customer has already rented this movie it give a error
        if (rentalCheck.length > 0) return res.status(400).send('The customer has already rented the movie: ' + rentalInput["movie"] );

        // Query | Insert a Rental
        await db.promise().query("INSERT INTO `rental` (`movie`, `customerId`, `dateReturned`, `rentalFee`) VALUES (?, ?, ?, ?)",
            [rentalInput["movie"], rentalInput["customerId"], rentalInput["dateReturned"], rentalInput["rentalFee"]]);

        await removeMovieInStock(rentalInput, res)

        // Query | Get one Rental with the rentalId
        const selectRental = await db.promise().query("SELECT * FROM `rental` WHERE movie = ? AND customerId = ?", [rentalInput["movie"], rentalInput["customerId"]]);

        const rental = selectRental[0];

        //  Error Message | There is no Rental
        if (rental.length < 1) return res.status(400).send('The rental with that given movie and customerId was not found...');

        // Ok Response | New Rental
        res.status(200).send(rental[0])

    } catch (err) {
        res.send(new Error("Error: " + err.message));
    }
});

// Update a Rental
router.put('/:movie/:customerId', async (req, res) => {
    try {
        const rentalInput = req.body;
        const movie = req.params.movie;
        const customerId = req.params.customerId;

        // Validate Error
        const {error} = validate(rentalInput)
        // Error Message | If validateCustomer has a error
        if (error) return res.status(400).send(error.details[0].message);

        // Query | Select a Movie with the customerId
        const selectRental = await db.promise().query("SELECT * FROM `rental` WHERE movie = ? AND customerId = ?", [movie, customerId]);
        const rental = selectRental[0];
        //  Error Message | There is no customer
        if (rental.length < 1) return res.status(400).send('The genre with that given ID was not found...');

        await movieExists(rentalInput, res);
        await customerExists(rentalInput, res);

        // Query | Update a Movie
        await db.promise().query("UPDATE `rental` SET `movie` = ?, `customerId` = ?, dateReturned = ?, rentalFee = ? WHERE movie = ? AND customerId = ?",
            [rentalInput["movie"], rentalInput["customerId"], rentalInput["dateReturned"], rentalInput["rentalFee"],  movie, customerId]);

        // Query | Get one Movie with the customerId
        const selectRental_ = await db.promise().query("SELECT * FROM `rental` WHERE movie = ? AND customerId = ?", [movie, customerId]);
        const rental_ = selectRental_[0];

        //  Error Message | There is no Rental
        if (rental_.length < 1) return res.status(400).send('The rental with that given ID was not found...');

        // Ok Response | Updated Movie
        res.status(200).send(rental_[0])
    } catch (err) {
        res.send(new Error("Error: " + err.message));
    }
});

// Delete a Rental
router.delete('/:movie/:customerId', async (req, res) => {
    try {
        const movie = req.params.movie;
        const customerId = req.params.customerId;

        // Query | Select a Rental with the rentalId
        const selectRental = await db.promise().query("SELECT * FROM `rental` WHERE movie = ? AND customerId = ?", [movie, customerId]);
        const rental = selectRental[0];
        //  Error Message | There is no Rental

        if (rental.length < 1) return res.status(400).send('The rental with that given ID was not found...');

        // Query | Delete a Rental with the rentalId
        await db.promise().query("DELETE FROM `rental` WHERE movie = ? AND customerId = ?", [movie, customerId]);

        await addMovieInStock(movie)

        // Ok Response | Deleted Rental
        res.status(200).send("Rental Deleted")
    } catch (err) {
        res.send(new Error("Error: " + err.message));
    }
})


async function movieExists(rentalInput, res) {
    // Query | Select a Movie with the movieId
    const selectMovie = await db.promise().query("SELECT * FROM `movie` WHERE `title` = ?", [rentalInput["movie"]]);
    const movie = selectMovie[0];
    //  Error Message | There is no Movie
    if (movie.length < 1) return res.status(400).send('The movie with the title: ' + rentalInput["movie"] + "doesn't exist...");
}

async function customerExists(rentalInput, res) {
    // Query | Select a Customer with the customerId
    const selectCustomer = await db.promise().query("SELECT * FROM `customer` WHERE `id` = ?", [rentalInput["customerId"]]);
    const customer = selectCustomer[0];
    //  Error Message | There is no Movie
    if (customer.length < 1) return res.status(400).send('The customer with the id: ' + rentalInput["customerId"] + "doesn't exist...");
}

async function addMovieInStock(movie) {
// Query | Select a Movie with the movieId
    const selectMovie = await db.promise().query("SELECT `numberInStock` FROM `movie` WHERE `title` = ?", [movie]);
    const movie1 = selectMovie[0];

    const newNumberInStock = movie1[0]["numberInStock"] + 1;

    // Query | Update a Movie
    await db.promise().query("UPDATE `movie` SET `numberInStock` = ? WHERE title = ?", [newNumberInStock, movie]);
}

async function removeMovieInStock(rentalInput, res) {
    // Query | Select a Movie with the movieId
    const selectMovie = await db.promise().query("SELECT `numberInStock` FROM `movie` WHERE `title` = ?", [rentalInput["movie"]]);
    const movie = selectMovie[0];

    //  Error Message | There is no Movie
    if (movie[0]["numberInStock"] < 1) return res.status(400).send('The movie: ' + rentalInput["movie"] + ' is out of stock...')

    const newNumberInStock = movie[0]["numberInStock"] - 1;
    // Query | Update a Movie
    await db.promise().query("UPDATE `movie` SET `numberInStock` = ? WHERE title = ?", [newNumberInStock, rentalInput["movie"]]);
}


module.exports = router;