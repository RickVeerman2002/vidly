const validate = require('../models/customer');
const express = require('express');
const router = express.Router();
const db = require('../database');


// Get all Customers
router.get('/', async (req, res) => {
    try {
        // Query | All Customers
        const selectCustomers = await db.promise().query("SELECT * FROM `customer`");

        const customers = selectCustomers[0];
        //  Error Message | There are no Customers
        if (customers.length < 1) return res.status(404).send('There were no customers found...');

        // Ok Message | All Customers
        res.status(200).send(customers);
    } catch (err) {
        res.send(new Error("Error: " + err.message));
    }
});

// Get a Customer
router.get('/:id', async (req, res) => {
    try {
        const customerId = req.params.id;
        // Query | Get one Customer with the genreId
        const selectCustomer = await db.promise().query("SELECT * FROM `customer` WHERE id =?", [customerId]);

        const customer = selectCustomer[0];

        //  Error Message | There is no Customer
        if (customer.length < 1) return res.status(404).send('The customer with that given ID was not found...');

        // Ok Message | One Customer
        res.status(200).send(customer[0])
    } catch (err) {
        res.send(new Error("Error: " + err.message))
    }
});

// Create a Customer
router.post('/', async (req, res) => {
    try {
        const customerInput = req.body;
        // Validate Error
        const {error} = validate(customerInput)
        // Error Message | If validateCustomer has a error
        if (error) return res.status(400).send(error.details[0].message);

        // Query | Insert a Customer
        const insertCustomer = await db.promise().query("INSERT INTO `customer` (`name`) VALUES (?)", [customerInput["name"]]);

        const insertId = insertCustomer[0].insertId;
        // Query | Select a Genre with the insertId
        const selectCustomer = await db.promise().query("SELECT * FROM `customer` WHERE id = ?", [insertId]);

        const customer = selectCustomer[0];

        //  Error Message | There is no Customer
        if (customer.length < 1) return res.status(404).send('The customer with that given ID was not found...');

        // Ok Response | New Customer
        res.status(200).send(customer[0])

    } catch (err) {
        res.send(new Error("Error: " + err.message));
    }
});

// Update a Customer
router.put('/:id', async (req, res) => {
    try {
        const customerInput = req.body;
        const customerId = req.params.id;

        // Validate Error
        const {error} = validate(customerInput)
        // Error Message | If validateCustomer has a error
        if (error) return res.status(400).send(error.details[0].message);

        // Query | Select a customer with the customerId
        const selectCustomer = await db.promise().query("SELECT * FROM `customer` WHERE id = ?", [customerId]);
        const customer = selectCustomer[0];
        //  Error Message | There is no customer
        if (customer.length < 1) return res.status(404).send('The customer with that given ID was not found...');

        // Query | Update a customer
        await db.promise().query("UPDATE `customer` SET `name` = ? WHERE id = ?", [customerInput["name"],  customerId]);

        // Query | Get one Customer with the customerId
        const selectCustomer_ = await db.promise().query("SELECT * FROM `customer` WHERE id = ?", [customerId]);
        const customer_ = selectCustomer_[0];

        //  Error Message | There is no customer
        if (customer_.length < 1) return res.status(404).send('The customer with that given ID was not found...');

        // Ok Response | Updated Customer
        res.status(200).send(customer_[0])
    } catch (err) {
        res.send(new Error("Error: " + err.message));
    }
});

// Delete a Customer
router.delete('/:id', async (req, res) => {
    try {
        const customerId = req.params.id;

        // Query | Select a Customer with the customerId
        const selectCustomer = await db.promise().query("SELECT * FROM `genre` WHERE id = ?", [customerId]);
        const customer = selectCustomer[0];
        //  Error Message | There is no Customer
        if (customer.length < 1) return res.status(404).send('The customer with that given ID was not found...');

        // Query | Delete a Customer with the customerId
        await db.promise().query("DELETE FROM `genre`  WHERE id = ?", [customerId]);

        // Ok Response | Deleted Customer
        res.status(200).send("Customer Deleted")
    } catch (err) {
        res.send(new Error("Error: " + err.message));
    }
})

// Set Customer isGold to True
router.put('/isgoldtrue/:id', async (req, res) => {
    try {
        const customerInput = req.body;
        const customerId = req.params.id;

        // Query | Select a customer with the customerId
        const selectCustomer = await db.promise().query("SELECT * FROM `customer` WHERE id = ?", [customerId]);
        const customer = selectCustomer[0];
        //  Error Message | There is no customer
        if (customer.length < 1) return res.status(404).send('The customer with that given ID was not found...');

        // Error Message | Customer is already gold
        if (customer[0]["isGold"] === 1) return res.status(404).send("The customer is already Gold...")

        // Query | Set Customer isGod to true
        await db.promise().query("UPDATE `customer` SET `isGold` = ? WHERE id = ?", [1,  customerId]);

        // Query | Get one customer with the customerId
        const selectCustomer_ = await db.promise().query("SELECT * FROM `customer` WHERE id = ?", [customerId]);
        const customer_ = selectCustomer_[0];

        //  Error Message | There is no customer
        if (customer_.length < 1) return res.status(404).send('The customer with that given ID was not found...');

        // Ok Response | Updated Customer
        res.status(200).send("Customer set to Gold: " + customer_[0]["name"]);
    } catch (err) {
        res.send(new Error("Error: " + err.message));
    }
})

// Set Customer isGold to false
router.put('/isgoldfalse/:id', async (req, res) => {
    try {
        const customerInput = req.body;
        const customerId = req.params.id;

        // Query | Select a customer with the customerId
        const selectCustomer = await db.promise().query("SELECT * FROM `customer` WHERE id = ?", [customerId]);
        const customer = selectCustomer[0];
        //  Error Message | There is no customer
        if (customer.length < 1) return res.status(404).send('The customer with that given ID was not found...');

        // Error Message | Customer is already gold
        if (customer[0]["isGold"] === 0) return res.status(404).send("The customer is already Default...")

        // Query | Set Customer isGod to false
        await db.promise().query("UPDATE `customer` SET `isGold` = ? WHERE id = ?", [0,  customerId]);

        // Query | Get one customer with the customerId

        const selectCustomer_ = await db.promise().query("SELECT * FROM `customer` WHERE id = ?", [customerId]);
        const customer_ = selectCustomer_[0];

        //  Error Message | There is no customer
        if (customer_.length < 1) return res.status(404).send('The customer with that given ID was not found...');

        // Ok Response | Updated Customer
        res.status(200).send("Customer set to Default: " + customer_[0]["name"])
    } catch (err) {
        res.send(new Error("Error: " + err.message));
    }
})

module.exports = router;