const express = require('express');
const app = express();
const genres = require('./routes/genres');
const customers = require('./routes/customers');
const movies = require('./routes/movies');
const rentals = require('./routes/rentals');
const home = require('./routes/home')

app.use(express.json());
app.use(express.urlencoded({ extended: true}));
app.use('/api/genres', genres);
app.use('/api/customers', customers);
app.use('/api/movies', movies);
app.use('/api/rentals', rentals);
app.use('/', home)

const port = process.env.PORT || 3000
app.listen(port, () => console.log(`Listening on port ${port}....`));

