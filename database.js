const mysql = require('mysql2');

try {
    module.exports = mysql.createConnection({
        host: 'localhost',
        user: 'root',
        password: 'root',
        database: 'node_course_vidly'
    });
} catch (e) {
    console.log(new Error("Error: " + e));
}




